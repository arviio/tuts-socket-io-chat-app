const path = require('path');
const webpack = require('webpack');

var debug = process.env.NODE_ENV !== "production";
var SRC_DIR = path.join(__dirname, "public");
var DIST_DIR = path.join(__dirname, "public/js/dist/");

var config = {
        context: __dirname,
        devtool: debug ? "inline-sourcemap" : null,
        entry: [
            'webpack-hot-middleware/client?reload=true',
            path.join(SRC_DIR, "authenticated.js")
        ],
        output: {
        path: DIST_DIR,
        publicPath: '/',
        filename: "[name].js"
    },
    modules: {
        loaders: [
            {
                test: /\.js?/,
                include: SRC_DIR,
                loader: "babel-loader",
                exclude: /node_modules/,
                query: {
                    presets: ["es2015", "stage-2"]
                }
            }
        ]
    },
    plugins: debug ? [
        new webpack.HotModuleReplacementPlugin()
    ] : [
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.optimize.UglifyJsPlugin({ mangle: false, sourcemap: false }),
    ]
};

module.exports = config;

