var module3 = require('./module3.js');

exports.testingModule4 = function () {
    console.log('hey you are accessing module 4');

    var test = module3.testingModule3a();

    const anotherTest = `this is just e es2015 ${test}`;

    console.log(anotherTest);
};

exports.testingModule4a = function () {
    console.log('hey you are accessing module 4a');
};