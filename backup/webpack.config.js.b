import path from 'path';
import webpack from 'webpack';

var debug = process.env.NODE_ENV !== "production";
var SRC_DIR = path.join(__dirname, "public");
var DIST_DIR = path.join(__dirname, "public/js/dist");

export default {
  entry: [
    'webpack-hot-middleware/client',
    path.join(SRC_DIR, "authenticated.js")
  ],
  output: {
    path: DIST_DIR,
    filename: "[name].js"
  },
  plugins: [
    new webpack.NoErrorsPlugin(),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin()
  ],
  module: {
    loaders: [
      {
        test: /\.js$/,
        include: SRC_DIR,
        loaders: [ 'babel' ]
      }
    ]
  },
  resolve: {
    extentions: [ '', '.js' ]
  }
};
