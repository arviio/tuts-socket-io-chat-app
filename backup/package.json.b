{
  "name": "socket-io-chat",
  "version": "1.0.0",
  "description": "",
  "main": "server.js",
  "scripts": {
    "start": "nodemon --watch server.js --exec babel-node -- server.js",
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "",
  "license": "ISC",
  "dependencies": {
    "cors": "^2.7.1",
    "express": "*",
    "socket.io": "*"
  },
  "devDependencies": {
    "babel-cli": "^6.14.0",
    "babel-loader": "^6.2.5",
    "babel-preset-es2015": "^6.14.0",
    "babel-preset-stage-2": "^6.13.0",
    "nodemon": "^1.10.2",
    "webpack": "^1.13.2",
    "webpack-dev-middleware": "^1.6.1",
    "webpack-hot-middleware": "^2.12.2"
  }
}
