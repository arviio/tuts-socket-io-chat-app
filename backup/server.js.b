//create server
var express = require('express');
var cors = require('cors');
var app = express();

import webpack from 'webpack';
import webpackMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import webpackConfig from './webpack.config';

const compiler = webpack(webpackConfig);

console.log(webpackConfig.output.path);

app.use(webpackMiddleware(compiler, {
  hot: true,
  publicPath: webpackConfig.output.path,
  noInfo: false
}));
app.use(webpackHotMiddleware(compiler));

app.use(cors());

console.log('here');

var server = require('http').createServer(app);

//prepare socket io, make it listen to server
var io = require('socket.io').listen(server);
var users = [];
var connections = [];

var port = process.env.PORT || 3000;
server.listen(port);
console.log(`It is running *:${port}`);

//routing
app.get('/', function(req, res) {
	res.sendFile(__dirname + '/public/index.html');
});

//open a connection
io.sockets.on('connection', function(socket) {
	connections.push(socket);
	console.log('Connected: %s sockets connected:', connections.length);

	//disconnect
	socket.on('disconnect', function(data) {
		users.splice(users.indexOf(socket), 1);
		connections.splice(connections.indexOf(socket), 1);
		console.log('Disconnected: %s sockets connected:', connections.length);
	});

	//send message
	socket.on('send message', function(data) {
		console.log('send message:' + data);

		//emit this data back on client side socket 'new message'
		io.sockets.emit('new message', {msg: data, user: socket.username});
	});

	//new user
	socket.on('new user', function(data, callback) {
		callback(true);

		socket.username = data;
		users.push(socket.username);

		updateUsernames();
	});

	function updateUsernames() {
		io.sockets.emit('get users', users);
	}
});