const path = require('path');
const express = require('express');
const cors = require('cors');
const app = express();

const webpack = require('webpack');
const webpackMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const config = require('./webpack.config.js');

const compiler = webpack(config);
const middleware = webpackMiddleware(compiler, {
	publicPath: config.output.publicPath,
	contentBase: path.join(__dirname, "public"),
	stats: {
	  colors: true,
	  hash: false,
	  timings: true,
	  chunks: false,
	  chunkModules: false,
	  modules: false
	}
});

app.use(middleware);
app.use(webpackHotMiddleware(compiler));

app.use(cors());

console.log('here');

var server = require('http').createServer(app);

//prepare socket io, make it listen to server
var io = require('socket.io').listen(server);
var users = [];
var connections = [];

var port = process.env.PORT || 3000;
server.listen(port);
console.log(`Server running *:${port}`);

//routing
app.get('/', function(req, res) {
	res.sendFile(__dirname + '/public/index.html');
});

//open a connection - socket.io built in event required on back-end to do something with the connection
io.on('connection', function(socket) {
	connections.push(socket);
	console.log('Connected: %s sockets connected:', connections.length);
	console.log('Connected socket:', socket.id);
	console.log('Connected socket connected state:', socket.connected);
	console.log('Connected socket disconnected state:', socket.disconnected);
	console.log('Connected socket rooms:', socket.rooms);

	//disconnect - socket.io built in event
	socket.on('disconnect', function(data) {
		users.splice(users.indexOf(socket), 1);
		connections.splice(connections.indexOf(socket), 1);
		console.log('Disconnected: %s sockets connected - %s:', connections.length, socket.username);
	});

	socket.on('message', function(data) {
		console.log('Socket message received: %s', data);
	});

	//send message
	socket.on('send message', function(data) {
		console.log('send message:' + data);

		//emit this data back on client side socket 'new message'
		 //(io.emit === all connected client)
		//io.emit('new message', {msg: data, user: socket.username});

		//emit this data back on client side socket 'new message' except for sender
		//any socket variable must be passed from server to client as it cannot be accessed by client as-s e.g socket.room, socket.username
		//socket.broadcast.emit('new message', {msg: data, user: socket.username, room: socket.room});

		// sending to all clients in 'game' room(channel) except sender
		//socket.broadcast.to(socket.room).emit('new message', {msg: data, user: socket.username, room: socket.room});

		// sending to all clients in 'game' room(channel), include sender
		io.in(socket.room).emit('new message', {msg: data, user: socket.username, room: socket.room});


		//forcing disconnecting socket sample
		// setTimeout(function(){
		// 	socket.disconnect();
		// }, 10000);
	});

	//new user
	socket.on('new user', function(data, callback) {
		callback(true);

		//set username as new socket property
		socket.username = data;


		var rooms   = ['room-101', 'room-102'];
		socket.room = rooms[Math.floor(Math.random() * rooms.length)];

		//add this new user to a room
		socket.join(socket.room);

		users.push({username: socket.username, room: socket.room});
		updateUsernames(socket.room);
	});

	//Sometimes, you want to have the system set up to have a response from the server any time the server receives your messages. You can do this by sending functions to the server, which the server will then call “directly” and pass some data in to it.
	socket.on("thisIsATest", function(func) {
	    func("This is a test message");
	});

	function updateUsernames(room) {
		io.in(room).emit('get users', {users: users, room: room});
	}
});